const fs = require('fs');
const conf = { encoding: 'utf8'};

var info = {
    path: '',
    type: 'undefined',
    content: 'undefined',
    childs: 'undefined'
};

const pathInfo = function(path, callback) {
    fs.stat(path, function(err, stats) {
        if (err) {
            callback(err, undefined);
            return;
        }
        if (stats.isDirectory()) {
            fs.readdir(path, function(err, items) {
                if (err) { 
                  callback(err); 
                  return; 
                }
                info.type = 'directory';
                info.path = path;
                info.childs = items;
                callback(null, info);
            });

        }
        else if (stats.isFile()) {
            fs.readFile(path, conf, function(err, content) {
                if (err) { 
                  callback(err); 
                  return; 
                }
                info.type = 'file';
                info.path = path;
                info.content = content;
                callback(null, info);
            });
        }
    });
};

module.exports = pathInfo;