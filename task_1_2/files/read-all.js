const fs = require('fs');
const conf = {encoding: 'utf8'};

const getContent = function(file) {
    return new Promise(function(done,fail) {
        fs.readFile(file, conf, function(err, content) {
            if (err) {
                fail(err);
            } else {
                done(content);
            }
        })
    })
};

const readAll = function(path) {
    var promise = new Promise(function(done, fail){
        fs.readdir(path, function(err, files) {
            if (err) {
                fail(err)
            } else {
                done(files)
            }
        })
    }).then(function(files){
        return Promise.all(
            files.map(function(file) {
                return getContent(path + file)
                    .then(function(text) {
                        return {name: file, content: text}
                    })
                    .catch(
                        function(err) {
                            return {name: file, content: 'not_found'}
                        }
                    );
            })
        )
    });

    return promise;
};

module.exports = readAll;