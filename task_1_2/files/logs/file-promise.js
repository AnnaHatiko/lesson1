const fs = require('fs');
const conf = {encoding: 'utf8'};

const read = function(file) {
  return new Promise( function(done,fail) {
      fs.readFile(file, conf, function(err, content) {
          if (err) {
              fail(err);
          } else {
              done(content);
          }
      })
  })
};
const write = function(file, data){
    return new Promise(function(done, fail) {
        fs.writeFile(file, data, function(err) {
            if (err) {
                fail(err);
            } else {
                done(file);
            }
        })
    })
};
module.exports = {read: read, write: write};
