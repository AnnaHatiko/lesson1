const readAll = require('./read-all');

function show(file) {
    console.log('-'.repeat(10));
    console.log(`Содержимое файлы ${file.name}:`);
    console.log(file.content);
    console.log('-'.repeat(10));
}

const path = './logs/';
readAll(path)
    .then(list => list.forEach(show))
    .catch(
        function(err) {
            console.error(err);
        }
    );
